#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#define RED "\x1B[31m"
#define RESET "\x1B[0m"
int main()
{
    srand(time(0));
    int n = 0, m = 0, k = 0, l = 0, js = 0, is=0;
    printf("Please,enter n and m(size of matrix)\n");
    scanf("%i%i", &n, &m);
    printf("Please,enter k and l\n");
    scanf("%i%i", &k, &l);
    int mat[n][m];
    int sor[n][m];
    bool unic[n * m];
    for (int i = 0; i < n * m; i++)
    {
        unic[i] = false;
    }

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            mat[i][j] = rand() % (n * m + 1) + 0;
            while (unic[mat[i][j]] == true)
            {
                mat[i][j] = rand() % (n * m + 1) + 0;
            }
            unic[mat[i][j]] = true;
            if ((j % 2 == 1) && ((mat[i][j] / k) < l))
            {
                sor[i][j] = 1;
                printf(RED "\t%i" RESET, mat[i][j]);
            }
            else
            {
                printf(RESET "\t%i" RESET, mat[i][j]);
                sor[i][j] = 0;
            }
        }
        printf("\n");
    }
    printf("\n");
    bool sorted = false;
    while (sorted == false)
    {
        sorted = true;
        int xb = -1, yb = -1;
        for (int i = 1; i < m; i += 2)
        {
            for (int j = 0; j < n; j++)
            {
                if (sor[j][i] == 1)
                {
                    xb = j;
                    yb = i;
                    break;
                }
            }
            if (xb > -1)
                break;
        }
        js = yb;
        is = xb;
        for (int i = js; i < m; i++)
        {
            for (int j = is; j < n; j++)
            {
                if (sor[j][i] == -1)
                    break;
                if (sor[j][i] == 1)
                {
                    sor[xb][yb] = 1;
                    sor[j][i] = -1;

                    if (mat[j][i] > mat[xb][yb])
                    {
                        sorted = false;
                        int tmp = mat[j][i];
                        mat[j][i] = mat[xb][yb];
                        mat[xb][yb] = tmp;
                    }
                    xb = j;
                    yb = i;
                }
            }
            is=0;
        }
        if (sorted == true)
            break;
        sorted = true;
        xb = -1, yb = -1;
        int st = m;
        if (st % 2 == 1)
            st--;
        for (int j = st - 1; j > 0; j -= 2)
        {
            for (int i = n - 1; i >= 0; i--)
            {
                if (sor[i][j] == 1)
                {
                    xb = i;
                    yb = j;
                    break;
                }
            }
            if (xb != -1)
                break;
        }
        js = yb;
        is = xb;
        for (int j = js; j > 0; j -= 2)
        {
            for (int i = is ; i >= 0; i--)
            {
                if (sor[i][j] == -1)
                    break;
                if (sor[i][j] == 1)
                {
                     sor[xb][yb] = 1;
                    sor[i][j] = -1;
                    if (mat[i][j] < mat[xb][yb])
                    {
                        sorted = false;
                        int tmp = mat[i][j];
                        mat[i][j] = mat[xb][yb];
                        mat[xb][yb] = tmp;
                    }
                    xb = i;
                    yb = j;
                }
            }
            is=n-1;
        }
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            if (j % 2 == 1 && ((sor[i][j] ==-1)||(sor[i][j] ==1)))
            {
                printf(RED "\t%i " RESET, mat[i][j]);
            }
            else
                printf(RESET "\t%i " RESET, mat[i][j]);
        }
        printf("\n");
    }
}