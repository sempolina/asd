#include <iostream>
#include <string>
#include <ctype.h>
#include <math.h>
using namespace std;

struct AuthorList
{
    string Name;
    AuthorList *next = nullptr;
};
struct book_value
{
    string title;
    AuthorList *authors = nullptr;
    int yearOfPublishing;
};
struct BookId
{
    int id = 0;
    BookId *next = nullptr;
};
struct Authors
{
    string Name = "";
    BookId *bookId = nullptr;
    void deinit()
    {
        BookId *node = bookId;
        while (node != nullptr)
        {
            BookId *next = node->next;
            delete node;
            node = next;
            next = nullptr;
        }
    }
};
struct Book
{
    int bookId = 0;
    book_value v;
    void deinit()
    {
        AuthorList *node = v.authors;
        while (node != nullptr)
        {
            AuthorList *next = node->next;
            delete node;
            node = next;
            next = nullptr;
        }
    }
};
class Hashtable_author
{
    Authors *table = nullptr;
    int loadness = 0;
    int size = 0;

public:
    Hashtable_author()
    {
        table = new Authors[10];
        size = 10;
    }
    ~Hashtable_author()
    {
        for (int i = 0; i < size; i++)
        {
            table[i].deinit();
        }
        delete[] table;
    }
    void rehashing()
    {
        Authors a[size];
        for (int i = 0; i < size; i++)
        {
            a[i] = table[i];
        }
        delete[] table;
        table = new Authors[size * 2];
        size *= 2;
        loadness = 0;
        for (int i = 0; i < size / 2; i++)
        {
            if (a[i].Name != "")
            {
                this->insert(a[i].Name, a[i].bookId);
            }
        }
    }
    int hashCode(string Name)
    {
        int size = Name.length();
        int code = 0;
        for (int i = 0; i < size; i++)
        {
            if (Name[i] == ' ')
            {
                code += (27 * pow(27, size - i - 1));
            }
            else
            {
                code += (tolower(Name[i]) - 'a' + 1) * pow(27, (size - i - 1));
            }
        }
        return code;
    }
    int getHash(string Name)
    {
        if (loadness != 0)
        {
            if (size / loadness < 2)
            {
                this->rehashing();
            }
        }

        int code = this->hashCode(Name);
        int hash = code % 7;
        bool empty = false;
        int i = 0;
        while (!empty)
        {
            hash += (i * (code % 5 + 1));
            if (table[hash % size].Name == "" || table[hash % size].Name == Name)
            {
                hash %= size;
                empty = true;
            }
            else
            {
                hash = code % 7;
            }
            i++;
        }
        return hash;
    }
    bool exist(string Name)
    {
        int code = this->hashCode(Name);
        int hash = code % 7;
        while (table[hash % size].Name != "")
        {
            if (table[hash % size].Name == Name)
                return true;
            hash += (code % 5 + 1);
        }
        return false;
    }
    void insert(string Name, BookId *bookId)
    {
        if (this->exist(Name))
        {
            BookId *id = this->find(Name);
            BookId *node = id;
            while (node->next != nullptr)
            {
                node = node->next;
            }
            node->next = bookId;
        }
        else
        {
            int hash = this->getHash(Name);
            loadness++;
            table[hash].Name = Name;
            table[hash].deinit();
            table[hash].bookId = bookId;
        }
    }
    BookId *find(string Name)
    {
        int code = this->hashCode(Name);
        int hash = code % 7;
        while (table[hash % size].Name != Name)
        {
            hash += (code % 5 + 1);
        }
        return table[hash % size].bookId;
    }
};
class Hashtable
{
    Book *table = nullptr;
    int loadness = 0;
    int size = 0;

public:
    Hashtable()
    {
        table = new Book[10];
        size = 10;
    }
    ~Hashtable()
    {
        for (int i = 0; i < size; i++)
        {
            table[i].deinit();
        }
        delete[] table;
    }
    void rehashing()
    {
        cout << "Table uses rehashing" << endl;
        Book b[size];
        for (int i = 0; i < size; i++)
        {
            b[i] = table[i];
        }
        delete[] table;
        table = new Book[size * 2];
        size *= 2;
        loadness = 0;
        for (int i = 0; i < size / 2; i++)
        {
            if (b[i].bookId > 0)
            {
                this->insert(b[i].bookId, b[i].v);
            }
        }
    }
    int hashCode(int key)
    {
        return key;
    }
    int getHash(int key)
    {
        if (loadness != 0)
        {
            if (size / loadness < 2)
            {
                this->rehashing();
            }
        }

        int code = this->hashCode(key);
        int hash = code % 7;
        bool empty = false;
        int i = 0;
        while (!empty)
        {
            hash += (i * (code % 5 + 1));
            if (table[hash % size].bookId < 1 || table[hash % size].bookId == key)
            {
                hash %= size;
                empty = true;
            }
            else
            {
                hash = code % 7;
            }
            i++;
        }
        return hash;
    }
    bool exist(int key)
    {
        int code = this->hashCode(key);
        int hash = code % 7;
        int i = 0;
        while (table[hash % size].bookId != 0)
        {
            if (table[hash % size].bookId == key)
                return true;
            if (i == size)
            {
                break;
            }
            hash += (code % 5 + 1);
            i++;
        }
        return false;
    }
    void insert(int key, book_value v)
    {
        int hash = this->getHash(key);
        table[hash].bookId = key;
        table[hash].deinit();
        table[hash].v = v;
        loadness++;
    }
    void remove(int key)
    {
        int code = this->hashCode(key);
        int hash = code % 7;
        while (table[hash % size].bookId != key)
        {
            hash += (code % 5 + 1);
        }
        loadness--;
        table[hash % size].bookId = -1;
    }
    Book find(int key)
    {
        int code = this->hashCode(key);
        int hash = code % 7;
        while (table[hash % size].bookId != key)
        {
            hash += (code % 5 + 1);
        }
        return table[hash % size];
    }
    void print()
    {
        for (int i = 0; i < size; i++)
        {
            if (table[i].bookId < 1)
                cout << "Empty" << endl;
            else
            {
                cout << table[i].bookId << " | " << table[i].v.title << " | " << table[i].v.yearOfPublishing << " | ";
                AuthorList *node = table[i].v.authors;
                while (node != nullptr)
                {
                    cout << node->Name;
                    if (node->next != nullptr)
                    {
                        cout << ",";
                    }
                    node = node->next;
                }
                cout << endl;
            }
        }
    }
};
int main()
{
    Hashtable hash;
    Hashtable_author auth;
    int com;
    cout << "Choose command:\n1.Add new element\n2.Delete element\n3.Find element\n4.Print table\n5.Find all Books\n6.Exit\n7.Auto adding" << endl;
    cin >> com;
    while (com != 6)
    {
        switch (com)
        {
        case 1:
        {
            Book b;
            cout << "Enter Id,please" << endl;
            cin >> b.bookId;
            cout << "Please,enter title" << endl;
            cin >> ws;
            getline(cin, b.v.title);
            cout << "Please, enter year of publishing" << endl;
            cin >> b.v.yearOfPublishing;
            cout << "Please, enter authors. If you entered all authors, please enter \"stop\"" << endl;
            string name;
            b.v.authors = nullptr;
            cin >> ws;
            getline(cin, name);
            while (name != "stop")
            {
                AuthorList *node = b.v.authors;
                AuthorList *pnode = new AuthorList;
                pnode->Name = name;
                if (node != nullptr)
                {
                    while (node->next != nullptr)
                    {
                        node = node->next;
                    }
                    node->next = pnode;
                }
                else
                {
                    b.v.authors = pnode;
                }
                BookId *id = new BookId;
                id->id = b.bookId;
                auth.insert(name, id);
                cin >> ws;
                getline(cin, name);
            }
            hash.insert(b.bookId, b.v);

            break;
        }
        case 2:
        {
            int key;
            cout << "Enter key,please" << endl;
            cin >> key;
            if (!hash.exist(key))
                cout << "This element doesn`t exist" << endl;
            else
            {
                hash.remove(key);
                hash.print();
            }
            break;
        }
        case 3:
        {
            int key;
            cout << "Enter key,please" << endl;
            cin >> key;
            if (!hash.exist(key))
                cout << "This element doesn`t exist" << endl;
            else
            {
                Book b = hash.find(key);
                cout << b.bookId << " | " << b.v.title << " | " << b.v.yearOfPublishing << " | ";
                AuthorList *node = b.v.authors;
                while (node != nullptr)
                {
                    cout << node->Name;
                    if (node->next != nullptr)
                    {
                        cout << ",";
                    }
                    node = node->next;
                }
                cout << endl;
            }
            break;
        }
        case 4:
        {
            hash.print();
            break;
        }
        case 5:
        {
            cout << "Please, enter author name" << endl;
            string Name;
            cin >> ws;
            getline(cin, Name);
            if (!auth.exist(Name))
                cout << "This author doesn`t exist";
            else
            {
                BookId *b = auth.find(Name);
                while (b != nullptr)
                {
                    Book book = hash.find(b->id);
                    cout << book.bookId << " | " << book.v.title << " | " << book.v.yearOfPublishing << endl;
                    b = b->next;
                }
            }
            break;
        }
        case 7:
        {
            for (int i = 1; i < 4; i++)
            {
                Book b;
                b.bookId = i;
                
                string name;
                if (i == 1)
                {
                    b.v.title = "My book";
                    name = "A S";
                    b.v.yearOfPublishing=1986;
                }
                else if (i == 2)
                {
                    b.v.title = "Birds";
                    name = "L M";
                    b.v.yearOfPublishing=1756;
                }
                else
                {
                    b.v.title = "Cats";
                    name = "C K";
                    b.v.yearOfPublishing=1824;
                }
                b.v.authors = nullptr;
                AuthorList *pnode = new AuthorList;
                pnode->Name = name;
                b.v.authors = pnode;
                BookId *id = new BookId;
                id->id = b.bookId;
                auth.insert(name, id);
                hash.insert(b.bookId, b.v);
            }
            break;
        }
        default:
            cout << "Incorrect command,try again" << endl;
            break;
        }
        cout << "Choose command:\n1.Add new element\n2.Delete element\n3.Find element\n4.Print table\n5.Find all Books\n6.Exit" << endl;
        cin >> com;
    }
}
