#include <iostream>

using namespace std;
class Heap
{
    int *arr;
    int n = 0;
    int size = 0;

public:
    Heap()
    {
        arr = new int[10];
        n = 10;
    }
    ~Heap()
    {
        delete[] arr;
    }

    void resize()
    {
        int *tmp = new int[n * 2];
        for (int i = 0; i < size; i++)
        {
            tmp[i] = arr[i];
        }
        delete[] arr;
        arr = tmp;
        n = n * 2;
    }
    void print()
    {
        int i = 0;
        int prev = 1;
        int p = 0;
        while (i < size)
        {
            while ((i < prev) && (i < size))
            {
                for (int k = 0; k < p; k++)
                {
                    cout << "-";
                }
                cout << ">" << arr[i] << endl;
                i++;
            }
            p++;
            prev = prev * 2 + 1;
        }
        cout << endl;
    }
    bool isheap()
    {
        for (int i = 1; i < size; i++)
        {
            int l = (i - 1) / 2;
            if (arr[i] < arr[l])
            {
                return false;
            }
        }
        return true;
    }
    void heapify()
    {
        for (int i = 1; i < size; i++)
        {
            int l = (i - 1) / 2;
            if (arr[i] < arr[l])
            {
                int k = i;
                while (((k - 1) / 2) >= 0)
                {
                    int m = (k - 1) / 2;
                    if (arr[k] < arr[m])
                    {
                        int tmp = arr[k];
                        arr[k] = arr[m];
                        arr[m] = tmp;
                        k = m;
                        this->print();
                    }
                    else
                        break;
                }
            }
        }
    }
    void push(int v)
    {
        if (size == n)
        {
            this->resize();
        }
        arr[size] = v;
        size++;
        this->print();
        if (!this->isheap())
        {
            cout << "It is not binary heap" << endl;
            this->heapify();
        }
    }
};
int main()
{
    Heap h;
    int com = 0;
    cout << "Enter, please, command:\n1. Add element\n2. Example\n3. Exit" << endl;
    cin >> com;
    while (com != 3)
    {
        if (com == 1)
        {
            cout << "Enter value, please:" << endl;
            int v;
            cin >> v;
            h.push(v);
        }
        else if(com==2)
        {
            h.push(12);
            h.push(156);
            h.push(10);
            h.push(8);
        }
        else
        {
            cout << "Incorrect command. Please try again.\n"
                 << endl;
        }
        cout << "Enter, please, command:\n1. Add element\n2. Example\n3. Exit" << endl;
        cin >> com;
    }
}