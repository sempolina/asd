#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
struct SLNode
{
    int data;
    struct SLNode *next;
};
void printSLList(struct SLNode *head)
{
    struct SLNode *node = head;
    while (node != NULL)
    {
        printf("%i ", node->data);
        node = node->next;
    }
    printf("\n");
}
struct SLNode *createSLNode(int data)
{
    struct SLNode *pnode = malloc(sizeof(struct SLNode));
    pnode->data = data;
    pnode->next = NULL;
    return pnode;
}
struct SLNode *addSLNode(struct SLNode *head, int data, int size)
{
    struct SLNode *node = createSLNode(data);
    if (size == 0)
        head = node;
    else
    {
        struct SLNode *next = head;
        while (next->next != NULL)
            next = next->next;
        next->next = node;
    }
    return head;
}
void deinit(struct SLNode *head)
{
    struct SLNode *node = head;
    while (node != NULL)
    {
        struct SLNode *next = node->next;
        free(node);
        node = next;
        next = NULL;
    }
}
struct SLNode *get(struct SLNode *head, int index)
{
    struct SLNode *node = head;
    for (int i = 0; i < index; i++)
        node = node->next;
    return node;
}
struct SLNode *cycle(struct SLNode *a2, struct SLNode *b2, struct SLNode **h2)
{
    if (b2 != NULL)
    {
        while (a2->data < b2->data)
        {
            (*h2)->data = a2->data;
            (*h2) = (*h2)->next;
            a2 = a2->next;
            if (a2 == NULL)
                break;
        }
    }
    else
    {
        while (a2 != NULL)
        {
            (*h2)->data = a2->data;
            (*h2) = (*h2)->next;
            a2 = a2->next;
        }
    }
    return a2;
}
struct SLNode *sort(struct SLNode *head, int n, int m)
{
    if (n < m)
    {
        struct SLNode *node = head;
        for (int i = 0; i < n; i++)
        {
            int key = node->data;
            int j = i - 1;
            int g= get(head, j)->data;
            while (j >= 0 && g > key)
            {
                struct SLNode *nj = get(head, j);
                struct SLNode *nn = nj->next;
                nn->data = nj->data;
                j = j - 1;
                nj->data = key;
                g= get(head, j)->data;
            }
            node = node->next;
        }
    }
    else
    {

        if (n > 1)
        {
            int k = n / 2;
            struct SLNode *a = NULL;
            struct SLNode *b = NULL;
            for (int i = 0; i < k; i++)
            {
                int g=get(head, i)->data;
                a = addSLNode(a,g, i);
            }
            for (int i = k; i < n; i++)
            {
                int g=get(head, i)->data;
                b = addSLNode(b,g, i - k);
            }
            a = sort(a, k, m);
            b = sort(b, n - k, m);
            struct SLNode *a2 = a;
            struct SLNode *b2 = b;
            struct SLNode *h2 = head;
            while (h2 != NULL)
            {
                if (a2 != NULL)
                    a2 = cycle(a2, b2, &h2);
                if (b2 != NULL)
                    b2 = cycle(b2, a2, &h2);
            }
            deinit(a);
            deinit(b);
        }
    }
    return head;
}
int main()
{
    int n;
    int m;
    printf("Enter,please,n and m\n");
    scanf("%i", &n);
    while (n <= 0)
    {
        printf("Incorrect n.Try again\n");
        scanf("%i", &n);
    }
    scanf("%i", &m);
    struct SLNode *head = NULL;
    int com;
    printf("Choose command:\n1. Random\n2. Enter numbers\n");
    scanf("%i", &com);
    while (com != 1 && com != 2)
    {
        printf("Incorrect command,try again!\n");
        printf("Choose command:\n1. Random\n2. Enter numbers\n");
        scanf("%i", &com);
    }
    if (com == 2)
    {
        for (int i = 0; i < n; i++)
        {
            int a;
            scanf("%i", &a);
            head = addSLNode(head, a, i);
        }
        for (int i = 0; i < n - 1; i++)
        {
            for (int j = i + 1; j < n; j++)
            {
                if (get(head, i)->data == get(head, j)->data)
                {
                    printf("Numbers are not unique!\n");
                    return 0;
                }
            }
        }
    }
    else
    {
        srand(time(0));
        bool unic[n];
        for (int i = 0; i < n; i++)
        {
            unic[i] = false;
        }
        for (int i = 0; i < n; i++)
        {
            head = addSLNode(head, rand() % (n) + 0, i);
            while (unic[get(head, i)->data] == true)
            {
                get(head, i)->data = rand() % (n + 1) + 0;
            }
            unic[get(head, i)->data] = true;
        }
    }
    printSLList(head);
    head = sort(head, n, m);
    printSLList(head);
    deinit(head);
}