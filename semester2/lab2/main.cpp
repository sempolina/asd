#include <iostream>
using namespace std;
struct queue
{
    int n = 5;
    int queue[5];
    int size = 0;
    bool rm = false;
};
void push_back(queue *q, int v)
{
    q->queue[q->size] = v;
    q->size++;
}
int pop(queue *q)
{
    q->size--;
    return q->queue[q->n - q->size-1];
}
void print(int arr[], int size)
{
    cout << "Queue:" << endl;
    for (int i = 0; i < size; i++)
        cout << arr[i] << " ";
    cout << endl;
}
int main()
{
    queue q;
    float val;
    cout << "Enter value,please:";
    cin >> val;
    while (val != 0)
    {
        int v = val;
        if (v == val)
        {
            if (!q.rm)
            {
                push_back(&q,v);
                print(q.queue, q.size);
                if (q.size == q.n)
                {
                    cout << "Queue is full" << endl;
                    q.rm = true;
                }
            }
            else
            {
                cout << "Deleted value:" <<pop(&q) << endl;
                print(&q.queue[q.n - q.size], q.size);
                if (!q.size)
                {
                    cout << "Queue is empty" << endl;
                    q.rm = false;
                }
            }
        }
        cin >> val;
    }
    cout << "Good bye!" << endl;
}