#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
struct SLNode
{
    int data;
    struct SLNode *next;
};
struct DLNode
{
    int data;
    struct DLNode *next;
    struct DLNode *prev;
};
void printSLList(struct SLNode *head)
{
    struct SLNode *node = head;
    while (node != NULL)
    {
        printf("%i ", node->data);
        node = node->next;
    }
    printf("\n");
}
void printDLList(struct DLNode *head)
{
    struct DLNode *node = head;
    while (node != NULL)
    {
        printf("%i ", node->data);
        node = node->next;
    }
    printf("\n");
}
int sizeSL(struct SLNode *head)
{
    int size = 0;
    struct SLNode *node = head;
    while (node != NULL)
    {
        size++;
        node = node->next;
    }
    return size;
}
int sizeDL(struct DLNode *head)
{
    int size = 0;
    struct DLNode *node = head;
    while (node != NULL)
    {
        size++;
        node = node->next;
    }
    return size;
}
struct SLNode *createSLNode(int data)
{
    struct SLNode *pnode = malloc(sizeof(struct SLNode));
    pnode->data = data;
    pnode->next = NULL;
    return pnode;
}
struct DLNode *createDLNode(int data)
{
    struct DLNode *pnode = malloc(sizeof(struct DLNode));
    pnode->data = data;
    pnode->next = NULL;
    pnode->prev = NULL;
    return pnode;
}
struct SLNode *addSLNode(struct SLNode *head, int data)
{
    struct SLNode *node = createSLNode(data);
    int size = sizeSL(head);
    if (size == 0)
        head = node;
    else if (node->data > head->data)
    {
        node->next = head;
        head = node;
    }
    else
    {
        struct SLNode *next = head;
        while (next->next != NULL)
            next = next->next;
        next->next = node;
    }
    return head;
}
struct DLNode *addDLNode(struct DLNode *head, int data)
{
    struct DLNode *new = createDLNode(data);
    int size = sizeDL(head);
    if (size == 0)
        head = new;
    else if (size == 1)
    {
        head->prev = new;
        new->next = head;
        head = new;
    }
    else
    {
        struct DLNode *node = head->next;
        struct DLNode *next = node->next;
        if (size != 2)
        {
            next->prev = new;
        }
        // next->prev = new;
        new->next = next;
        new->prev = node;
        node->next = new;
        node = NULL;
        next = NULL;
    }
    new=NULL;
    return head;
}
struct DLNode *removeDLNode(struct DLNode *head, struct SLNode **head2)
{
    struct DLNode *node = head;
    while (node != NULL)
    {
        if (node->data % 2 == 0)
        {
            struct DLNode *next = node->next;
            struct DLNode *prev = node->prev;
            *head2 = addSLNode(*head2, node->data);
            free(node);
            node = next;
            if (node != NULL)
                node->prev = prev;
            if (prev != NULL)
                prev->next = node;
            else
            {
                head = node;
            }
        }
        else
            node = node->next;
    }
    return head;
}
void deinit(struct DLNode *head, struct SLNode *head2)
{
    struct DLNode *node = head;
    while (node != NULL)
    {
        struct DLNode *next = node->next;
        node->prev = NULL;
        free(node);
        node = next;
        next = NULL;
    }
    struct SLNode *node2 = head2;
    while (node2!= NULL)
    {
        struct SLNode *next = node2->next;
        free(node2);
        node2 = next;
        next = NULL;
    }
}
int main()
{
    int com = 0;
    struct DLNode *head = NULL;
    struct SLNode *head2 = NULL;
    printf("Choose option:\n1.Add to list\n2.Remove from list\n3.Finish\n");
    scanf("%i", &com);
    while (com != 3)
    {
        if (com == 1)
        {
            printf("Enter data\n");
            int data = 0;
            scanf("%i", &data);
            head = addDLNode(head, data);
        }
        else
        {
            head = removeDLNode(head, &head2);
        }
        printf("\nSize of DLNode:%i\n", sizeDL(head));
        printf("DLNode:\n");
        printDLList(head);
        printf("\nSize of SLNode:%i\n", sizeSL(head2));
        printf("SLNode:\n");
        printSLList(head2);
        printf("\nChoose option:\n1.Add to list\n2.Remove from list\n3.Finish\n");
        scanf("%i", &com);
    }
    deinit(head, head2);
}