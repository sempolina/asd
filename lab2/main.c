#include <stdio.h>
#include <time.h>
#include <stdlib.h>
int main()
{
  srand(time(0));
  float s=0;
  printf("Please enter size:\n");
  scanf("%f",&s);
  int size=s;
  if(size!=s||size<=1)
  {
   printf("Incorrect size\n");
  }
  else
  {
      int mat[size][size];
      float com=0;
      printf("Choose:\n1.Control matrix\n2.Random matrix\n");
      scanf("%f",&com);
      if((com!=1)&&(com!=2))
      {
          printf("Incorrect command\n");
      }
     else 
      {
          int n=1;
         if(com==1)   
         {
             for(int i=0;i<size;i++)
             {
                 for(int j=0;j<size;j++)
                 {
                     mat[i][j]=n;
                     n++;
                     printf("%i ",mat[i][j]);
                 }
                 printf("\n");
             }

         } 
         else
         {
             for(int i=0;i<size;i++)
             {
                 for(int j=0;j<size;j++)
                 {
                     mat[i][j]=rand()%100+1;
                     n++;
                    printf("%i ",mat[i][j]);
                 }
                 printf("\n");
             }

         }
         int maxunder=mat[size-1][0],min=mat[size-1][size-1],max=mat[size-1][size-1],minup=mat[0][1];
         int xmaun=size-1,ymaun=0,mi=size-1,ma=size-1,xmiup=0,ymiup=1;
         for(int i=0;i<size;i=i+2)
         {
             for(int j=size-1;j>i;j--)
             {

                 printf("[%i,%i] %i   ",j,i,mat[j][i]);
                 if(mat[j][i]>maxunder)
                 {
                     maxunder=mat[j][i];
                     xmaun=j;
                     ymaun=i;
                 }
             }
             for(int j=2+i;j<size;j++)
             {
                  printf("[%i,%i] %i   ",j,i+1,mat[j][i+1]);
                 if(mat[j][i+1]>maxunder)
                 {
                     maxunder=mat[j][i+1];
                     xmaun=j;
                     ymaun=i+1;
                 }
             }
         }
         printf("\n");
         for(int i=size-1;i>=0;i--)
         {  
              printf("[%i,%i] %i    ",i,i,mat[i][i]);
             if(mat[i][i]>max)
             {
                 max=mat[i][i];
                 ma=i;
             }
             if(mat[i][i]<min)
             {
                 min=mat[i][i];
                 mi=i;     
             }
         }
         printf("\n");
         for(int i=0;i<size;i=i+2)
         {
             for(int j=i+1;j<size;j++)
             {
                  printf("[%i,%i] %i    ",i,j,mat[i][j]);
                 if(mat[i][j]<minup)
                 {
                     minup=mat[i][j];
                     xmiup=i;
                     ymiup=j;
                 }
             }
             for(int j=size-1;j>i+1;j--)
             {
                 printf("[%i,%i] %i       ",i+1,j,mat[i+1][j]);
                 if(mat[i+1][j]<minup)
                 {
                     minup=mat[i+1][j];
                     xmiup=i+1;
                     ymiup=j;
                 }
             }
         }
         printf("\n");
         printf("Maximal element under main diagonal:[%i][%i] %i\n",xmaun,ymaun,maxunder);
         printf("Maximal element of main diagonal:[%i][%i] %i\n",ma,ma,max);
         printf("Minimal element of main diagonal:[%i][%i] %i\n",mi,mi,min);
         printf("Minimal element below main diagonal:[%i][%i] %i\n",xmiup,ymiup,minup);
      }
  }
}